/*
 * FinalApplicationDetail Messages
 *
 * This contains all the text for the FinalApplicationDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.FinalApplicationDetail.header',
    defaultMessage: 'This is the FinalApplicationDetail component !',
  },
});
