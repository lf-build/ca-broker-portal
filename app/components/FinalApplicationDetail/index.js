/**
*
* FinalApplicationDetail
*
*/

import React from 'react';

class FinalApplicationDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { applicants, businessName, loanAmount, repaymentAmount, sellRate, term, commission } = this.props.applicationDetails;
    return (
      <div className="finalization_text">
        <ul className="final-app">
          <li className="remove_padd_bottom">
            <span className="span_left ">Applicant Name</span>
            <span className="span_right"><b>{applicants.map((applicant, index) => (<label htmlFor="applicant" key={index}>{applicant}</label>))}</b></span>
          </li>
          <li>
            <span className="span_left">Business Name</span>
            <span className="span_right">{businessName}</span>
          </li>
          <li>
            <span className="span_left">Loan Amount</span>
            <span className="span_right">${loanAmount}</span>
          </li>
          <li>
            <span className="span_left"> Repayment Amount</span>
            <span className="span_right">${repaymentAmount}</span>
          </li>
          <li>
            <span className="span_left"> Sell Rate</span>
            <span className="span_right">{sellRate}</span>
          </li>
          <li>
            <span className="span_left"> Term</span>
            <span className="span_right">{term}</span>
          </li>
          <li>
            <span className="span_left"><b>Commission </b></span>
            <span className="span_right"> <b>${commission}</b> </span>
          </li>
        </ul>
      </div>
    );
  }
}

FinalApplicationDetail.propTypes = {
  applicationDetails: React.PropTypes.object.isRequired,
};

export default FinalApplicationDetail;
