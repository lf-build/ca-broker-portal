/**
*
* LinkAccountingSystem
*
*/

import React from 'react';
import PrivacyPolicy from 'components/PrivacyPolicy';
import quickBooks from '../../assets/images/quickbooks_logo.png';
import xero from '../../assets/images/Xero-Logo-colour-2.png';
import wave from '../../assets/images/wave_logo.png';
import sageOne from '../../assets/images/sageonelogo2.png';
import other from '../../assets/images/other_system.png';

// import styled from 'styled-components';


class LinkAccountingSystem extends React.Component { // eslint-disable-line react/prefer-stateless-function
  openChildWindow = () => {
    this.props.apiCallStatus(1);
    const child = window.open('https://www.criskco.com/criskco-connect?refId=812DC', 'Crisko', '"directories=1,titlebar=1,toolbar=1,location=1,status=1,menubar=1,width=750,height=700,1,status=1');
    const checkChild = () => {
      if (child.closed) {
        setTimeout(this.props.loadData(), 5000);
        clearInterval(timer);
      }
    };
    const timer = setInterval(checkChild, 5000);
  };
  render() {
    const { skipCriskco } = this.props;
    return (
      <div className="col-md-6 account-contain">
        <div className="que-heading">
          <h2>Business Verification</h2>
        </div>
        <div className="progress-bars">
          <span className="circle active">1</span>
          <span className="line-d"></span>
          <span className="circle">2</span>
          <span className="line-d"></span>
          <span className="circle">3</span>
        </div>
        <div className="col-md-12">
          <div className="link-wrap">
            <div className="col-sm-7">
              <h4 className="ac-head">Bokkeeping Review</h4>
              <p className="ac-text"> we look at your business data, not just your credit score, to get you qualified quickly and easily.</p>
              <button className="account_btn" onClick={this.openChildWindow}>{ this.props.apiCall ? 'CONNECTING...' : 'CONNECT'}</button>
              <div className="col-sm-12">
              </div>
            </div>
            <div className="col-sm-5">
              <ul className="images-logo">
                <li><a> <img alt="" src={quickBooks} className="img-responsive" /></a></li>
                <li><a> <img alt="" src={xero} className="img-responsive" /></a></li>
                <li><a> <img alt="" src={wave} className="img-responsive" /></a></li>
                <li><a> <img alt="" src={sageOne} className="img-responsive" /></a></li>
                <li><a> <img alt="" src={other} className="img-responsive" /></a></li>
              </ul>
            </div>
            <div className="col-md-12">
              <p className="ac-text">Connecting helps you get funding quicker.<br />
                <a><button className="skipCrsk" type="button" onClick={() => skipCriskco()} >
                 Skip this step.</button></a>
              </p>
            </div>
          </div>
        </div>
        <PrivacyPolicy />
      </div>
    );
  }
}

LinkAccountingSystem.propTypes = {
  skipCriskco: React.PropTypes.any,
  apiCall: React.PropTypes.any,
  loadData: React.PropTypes.func,
  apiCallStatus: React.PropTypes.func,
};

export default LinkAccountingSystem;
