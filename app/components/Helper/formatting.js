export function phoneMasking(value) {
  let result = '';
  if (value) {
    const a = value.replace(/\D/g, '').match(/(\d{3})(\d{3})(\d{4})/);
    result = `(${a[1]}) ${a[2]}-${a[3]}`;
  }
  return result;
}

export function simplePhoneMasking(value) {
  let result = '';
  if (value) {
    const a = value.replace(/\D/g, '').match(/(\d{3})(\d{3})(\d{4})/);
    result = `${a[1]}-${a[2]}-${a[3]}`;
  }
  return result;
}
