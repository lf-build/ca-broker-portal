/**
*
* OwnerDetails
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { FieldArray, formValueSelector } from 'redux-form/immutable';
// import styled from 'styled-components';
import TextField from '@ui/TextField';
import DateField from '@ui/DateField';
import StateField from '@ui/StateField';
import SSNField from '@ui/SSNField';
import PhoneField from '@ui/PhoneField';
import ZipField from '@ui/ZipField';

import localityNormalizer from '@ui/Utils/localityNormalizer';

import messages from './messages';

import infoImg from '../../assets/images/info.png';
import addOwnerImg from '../../assets/images/add-plus-button.png';
import errorImg from '../../assets/images/error.png';

const parsedMessage = localityNormalizer(messages);

class OwnerDetails extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0, isDisplay: true };
    this.tabs = {};
  }
  componentDidMount() {
    window.activeIndex = 0;
  }


 RenderOwner = ({ fields, tabClick,activeIndex,removeMember,addMember, isDisplay,removeOwnershipAlet }) => ( // eslint-disable-line
   <div>
     <div className="upper-tab">
       <ul className="tabs">
         {fields.map((owner, index) =>
           <li key={index} >
             <div className={activeIndex === index ? 'tab-box active' : 'tab-box'}>
               {
                 <button onClick={() => tabClick(index, fields)} type="button" className="app-tab">{this.tabs[owner] ? `${this.tabs[owner].firstName} ${this.tabs[owner].lastName || ''}` : 'Application Details' }</button>
                }
               {
                index === 0 ? null :
                <button onClick={() => removeMember(fields, index)} type="button" className="close-btn"><img src={errorImg} alt="" /></button>
               }
             </div>
             {
                fields.length === 4 || fields.length !== index + 1
              ? null :
              <button type="button" onClick={() => addMember(fields)} className="app-tab">
                <div className="tab-img">
                  <img src={addOwnerImg} alt="" />
                </div>
              </button>
             }
           </li>
        )}
       </ul>
     </div>
     {fields.map((owner, index) =>
       <div key={index} className={activeIndex === index ? 'comman_application_wraper' : 'comman_application_wraper hideDiv'}>
         <div className="comman_applicationinner business-details ">
           <div className="col-md-4 col-sm-4">
             <TextField onChange={(e) => { (this.tabs[owner] || (this.tabs[owner] = {})).firstName = e.target.value; }} name={`${owner}.firstName`} {...parsedMessage.firstName} />
           </div>
           <div className="col-md-4 col-sm-4">
             <TextField onChange={(e) => { (this.tabs[owner] || (this.tabs[owner] = {})).lastName = e.target.value; }} name={`${owner}.lastName`} {...parsedMessage.lastName} />
           </div>
           <div className="col-md-4 col-sm-4">
             <SSNField name={`${owner}.ssn`} {...parsedMessage.ssn} />
           </div>
         </div>
         <div className="comman_applicationinner business-details">
           <div className="col-md-4 col-sm-4">
             <PhoneField name={`${owner}.primaryPhone`} {...parsedMessage.primaryPhone} />
           </div>
           <div className="col-md-4 col-sm-4">
             <PhoneField name={`${owner}.secondaryPhone`} {...parsedMessage.secondaryPhone} />
           </div>
           <div className="col-md-4 col-sm-4">
             <TextField name={`${owner}.emailAddress`} {...parsedMessage.emailAddress} />
           </div>
         </div>
         <div className="comman_applicationinner business-details">
           <div className="col-md-4 col-sm-4">
             <TextField name={`${owner}.homeAddress`} {...parsedMessage.homeAddress} />
           </div>
           <div className="col-md-4 col-sm-4">
             <TextField name={`${owner}.city`} {...parsedMessage.city} />
           </div>
           <div className="col-md-4 col-sm-4">
             <div className="col-xs-6 no-pad-left">
               <StateField name={`${owner}.state`} {...parsedMessage.state} />
             </div>
             <div className="col-xs-6 no-pad-left">
               <ZipField name={`${owner}.zipCode`} {...parsedMessage.zipCode} />
             </div>
           </div>
         </div>
         <div className="comman_applicationinner business-details">
           <div className="col-md-6 col-sm-12">
             <DateField
               dateFormat="mm/dd/yyyy"
               name={`${owner}.dateOfBirth`}
               label="Date of birth"
               placeholder="Date of birth"
               maxYears={100}
             />
           </div>
         </div>
         <div className="another_owner">
           <div className={isDisplay === true ? 'col-xs-12' : 'col-xs-12 hideDiv'}>
             <div className="col-md-8 col-sm-12">
               <p>
                 <img alt="" src={infoImg} className="img_left" />
               Applicants must have ownership of at least 75% of the business, cumulatively, to receive funding.</p>
               <button type="button" onClick={removeOwnershipAlet} className="close-img"><img src={errorImg} alt="" /></button>
             </div>
           </div>
         </div>
       </div>
   )}
   </div>
);
  removeMember=(fields, index) => {
    fields.remove(index);
    const reTabs = {};
    delete this.tabs[`owners[${index}]`];
    Object.keys(this.tabs).forEach((key, j) => {
      reTabs[`owners[${j}]`] = this.tabs[key];
    });
   // delete this.tabs;
    this.tabs = reTabs;
    if (index === this.state.activeIndex) {
      this.tabClick(index - 1);
    }
  }
  tabClick = (e) => {
    this.setState({ activeIndex: e });
  };

  addMember=(fields) => {
    fields.push({});
    this.setState({ activeIndex: fields.length });
  }
  removeOwnershipAlet= () => {
    this.setState({ isDisplay: false });
  };

  render() {
    return (
      <FieldArray addMember={this.addMember} removeOwnershipAlet={this.removeOwnershipAlet} isDisplay={this.state.isDisplay} removeMember={this.removeMember} activeIndex={this.state.activeIndex} tabClick={this.tabClick} name="owners" component={this.RenderOwner} />
    );
  }
}

OwnerDetails.propTypes = {};

const selector = formValueSelector('applicationForm'); // <-- same as form name

export default connect(
  (state) => {
    // can select values individually
    const owners = selector(state, 'owners');
    return { ...owners };
  }
)(OwnerDetails);
