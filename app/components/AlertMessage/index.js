/**
*
* AlertMessage
*
*/

import React from 'react';

class AlertMessage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      isVisible: true,
    };
  }
  render() {
    const { isVisible } = this.state;
    const { message, isError } = this.props;
    // Don't show anything is isVisible is false
    if (!isVisible) {
      return <span />;
    }
    if (isError) {
      return (
        <div className="alert alert-danger alert-dismissable fade in">
          <i className="fa fa-times-circle-o sucess_message" aria-hidden="true"></i>
          <button
            onClick={() => {
              if (this.props.onClose) {
                this.props.onClose();
              }
              this.setState({ isVisible: false });
            }} className="close" data-dismiss="alert" aria-label="close"
          >×</button>
          <strong>{message}</strong>
        </div>
      );
    }
    // If visible and not any error, show success message.
    return (
      <div className="alert alert-success alert-dismissable fade in">
        <i className="fa fa-check-square-o sucess_message" aria-hidden="true"></i>
        <button
          onClick={() => {
            if (this.props.onClose) {
              this.props.onClose();
            }
            this.setState({ isVisible: false });
          }} className="close" data-dismiss="alert" aria-label="close"
        >×</button>
        <strong>{message}</strong>
      </div>
    );
  }
}

AlertMessage.propTypes = {
  message: React.PropTypes.string,
  isError: React.PropTypes.bool,
  onClose: React.PropTypes.func,
  // isVisible: React.PropTypes.bool,
};

export default AlertMessage;
