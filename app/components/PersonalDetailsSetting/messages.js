/*
 * PersonalDetailsSetting Messages
 *
 * This contains all the text for the PersonalDetailsSetting component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.PersonalDetailsSetting.header',
    defaultMessage: 'This is the PersonalDetailsSetting component !',
  },
  firstNamePlaceholder: {
    id: 'app.components.BankDetailsSetting.firstNamePlaceholder',
    defaultMessage: 'First Name',
  },
  firstNameLabel: {
    id: 'app.components.BankDetailsSetting.firstNameTitle',
    defaultMessage: 'First Name',
  },
  lastNamePlaceholder: {
    id: 'app.components.BankDetailsSetting.lastNamePlaceholder',
    defaultMessage: 'Last Name',
  },
  lastNameLabel: {
    id: 'app.components.BankDetailsSetting.lastNameTitle',
    defaultMessage: 'Last Name',
  },
  dateOfBirthLabel: {
    id: 'app.components.BankDetailsSetting.dateOfBirthTitle',
    defaultMessage: 'Date Of Birth',
  },
  streetPlaceholder: {
    id: 'app.components.BankDetailsSetting.streetPlaceholder',
    defaultMessage: 'Street',
  },
  streetLabel: {
    id: 'app.components.BankDetailsSetting.streetTitle',
    defaultMessage: 'Street',
  },
  cityPlaceholder: {
    id: 'app.components.BankDetailsSetting.cityPlaceholder',
    defaultMessage: 'City',
  },
  cityLabel: {
    id: 'app.components.BankDetailsSetting.cityTitle',
    defaultMessage: 'City',
  },
  postCodePlaceholder: {
    id: 'app.components.BankDetailsSetting.postCodePlaceholder',
    defaultMessage: 'Postcode',
  },
  postCodeLabel: {
    id: 'app.components.BankDetailsSetting.postCodeTitle',
    defaultMessage: 'Postcode',
  },
  mobileNumberPlaceholder: {
    id: 'app.components.BankDetailsSetting.mobileNumberPlaceholder',
    defaultMessage: 'Mobile Phone Number',
  },
  mobileNumberLabel: {
    id: 'app.components.BankDetailsSetting.mobileNumberTitle',
    defaultMessage: 'Mobile Phone Number',
  },
  companyNamePlaceholder: {
    id: 'app.components.BankDetailsSetting.companyNamePlaceholder',
    defaultMessage: 'Company Name',
  },
  companyNameLabel: {
    id: 'app.components.BankDetailsSetting.companyNameTitle',
    defaultMessage: 'Company Name',
  },
});
