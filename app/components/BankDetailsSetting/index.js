/**
*
* BankDetailsSetting
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import Form from '@ui/Form';
import SelectField from '@ui/SelectField';
import Spinner from 'components/Spinner';
import AlertMessage from 'components/AlertMessage';
import { authCheck } from 'components/Helper/authCheck';
import makeAuthSelector from 'sagas/auth/selectors';
import { required } from 'lib/validation/required';
import { isInLength } from 'lib/validation/isInLength';
import messages from './messages';

class BankDetailsSetting extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      partnerId: props.auth.partnerId,
      isVisible: false,
    };
  }
  afterSubmitDone = (err, success) => {
    if (err) {
      this.setState({ isError: true, message: 'Error updating Bank details', isVisible: true });
      authCheck(err);
      return;
    }
    this.setState({ isError: false, message: 'Bank details updated successfully', isVisible: true, initialValues: success });
  };
  handleOnLoadError = (err) => {
    authCheck(err);
  };
  render() {
    const { auth: { user } } = this.props;
    if (!this.state) {
      return <Spinner />;
    }
    const parsedMessages = localityNormalizer(messages);
    return (
      <div className="application_wraper">
        <div className="container-fluid responsive-wrap">
          <div className=" col-sm-12">
            <h2>Settings</h2>
          </div>
          <div className="res-margin">
            <div className="comman_applicationinner">
              <div className="col-md-5 col-sm-6">
                <Form initialValuesBuilder={(values) => this.state.initialValues || values} afterSubmit={this.afterSubmitDone} loadAction={['on-boarding-broker/settings/get-bank-details', { partnerId: user.partnerId }]} name="bankDetails" action="on-boarding-broker/settings/set-bank-details" onLoadError={this.handleOnLoadError}>
                  <TextField {...parsedMessages.bankName} name="bankName" validate={[required('Bank Name'), isInLength(2, 100)]} />
                  <TextField {...parsedMessages.bankAccountName} name="bankAccountName" validate={[required('Bank Account Name'), isInLength(2, 100)]} />
                  <TextField {...parsedMessages.bankAccountNumber} name="bankAccountNumber" validate={[required('Bank Account Number'), isInLength(2, 100)]} />
                  <SelectField {...parsedMessages.accountType} name="accountType" label="Account Type" source={[{ value: 1, text: 'Saving' }, { value: 2, text: 'Checking' }]} />
                  <TextField {...parsedMessages.aba} name="aba" validate={[required('ABA #'), isInLength(2, 100)]} />
                  <button type="submit" className="btn next_btn">UPDATE</button>
                  {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
                  <TextField {...parsedMessages.bankId} name="bankId" type="hidden" />
                  <TextField {...parsedMessages.partnerId} name="partnerId" type="hidden" />
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BankDetailsSetting.propTypes = {
  auth: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

export default connect(mapStateToProps)(BankDetailsSetting);
