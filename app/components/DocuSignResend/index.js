/**
*
* DocuSignResend
*
*/

import React from 'react';
import Spinner from 'components/Spinner';
import { Link } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import docuSignImage from 'assets/images/docuSign01.png';
import checkedImg from 'assets/images/checked.png';
import { authCheck } from 'components/Helper/authCheck';


class DocuSignResend extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { resendText: 'Resend' };
  }
  resendDocuSignMail(applicationNumber) {
    this.setState({
      resendText: 'Resending...',
    });
    execute(undefined, undefined, ...'on-boarding-broker/agreement/resend-docusign'.split('/'), {
      applicationNumber,
    })
    .then(() => {
      this.setState({
        resendText: 'Resent',
      });
    }).catch((e) => {
      this.setState({ resendText: 'Resend Failed' });
      authCheck(e);
    }
    ); // eslint-disable-line
  }
  render() {
    const { email, docuSignStatus, applicationNumber } = this.props;
    return (
      <div className="col-sm-4">
        <div className="contract-box">
          <div className="legal-frm">
            <p>The legal forms were sent to:</p>
            <p>{email}</p>
          </div>
          <div className="doc-status">
            <div className="doc-img">
              <img src={docuSignImage} alt="" />
            </div>
            <div className="status-d">
              <span>Status:</span>
              <h4>{docuSignStatus}</h4>
            </div>
          </div>
          <div className="doc-txt">
            A copy has been sent to your inbox. <Link onClick={() => this.resendDocuSignMail(applicationNumber)}>{`${this.state.resendText} `}</Link>
            { this.state.resendText === 'Resent' && <span className="right-click active"><img src={checkedImg} alt="check" /></span>}
            { this.state.resendText === 'Resending...' && <Spinner isSmall /> }
          </div>
        </div>
      </div>
    );
  }
}

DocuSignResend.propTypes = {
  email: React.PropTypes.string,
  docuSignStatus: React.PropTypes.string,
  applicationNumber: React.PropTypes.string,
};

export default DocuSignResend;
