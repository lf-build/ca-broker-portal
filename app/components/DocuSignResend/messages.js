/*
 * DocuSignResend Messages
 *
 * This contains all the text for the DocuSignResend component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DocuSignResend.header',
    defaultMessage: 'This is the DocuSignResend component !',
  },
});
