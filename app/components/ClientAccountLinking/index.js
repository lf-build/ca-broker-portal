/**
*
* ClientAccountLinking
*
*/

import React from 'react';
import { Link } from 'react-router';
import linkingIcon from 'assets/images/linking-icon.png';


class ClientAccountLinking extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { applicationNumber, bankLinkingStatus } = this.props;
    let activeClass = 'verify-box active';
    if (!bankLinkingStatus) {
      activeClass = 'verify-box';
    }
    return (
      <div className={activeClass}>
        <div className="number-box">
          <span>2</span>
        </div>
        <div className="verify-head">
          <h4>The link in the email will lead to the <Link target="_blank" to={`/link/account/${applicationNumber}`}>account linking page.</Link></h4>
        </div>
        <div className="verify-img">
          <a className="cursor-default"><img src={linkingIcon} alt="icon" /></a>
        </div>
        <div className="verify-detail">
          <p>Have your client type in their Accounting &amp; Bank Log-in information.</p>
        </div>
      </div>
    );
  }
}

ClientAccountLinking.propTypes = {
  applicationNumber: React.PropTypes.string,
  bankLinkingStatus: React.PropTypes.bool,
};

export default ClientAccountLinking;
