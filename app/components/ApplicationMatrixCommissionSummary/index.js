/**
*
* ApplicationMatrixCommissionSummary
*
*/

import React from 'react';
import { FormattedNumber } from 'react-intl';
// import styled from 'styled-components';

class ApplicationMatrixCommissionSummary extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { short, commission: value } = this.props;
    if (short) {
      return (<a className="comminsion_btn">
        <FormattedNumber
          {...{ value, style: 'currency', maximumFractionDigits: 0, currency: 'USD' }}
        />
      </a>);
    }
    return (
      <div className="comminsion_section">
        <h1><FormattedNumber
          {...{ value, style: 'currency', maximumFractionDigits: 0, currency: 'USD' }}
        /></h1>
        <p>Commission Earned</p>
      </div>
    );
  }
}
ApplicationMatrixCommissionSummary.defaultProps = {
  short: false,
  commission: 0,
};
ApplicationMatrixCommissionSummary.propTypes = {
  short: React.PropTypes.bool,
  commission: React.PropTypes.number,
};

export default ApplicationMatrixCommissionSummary;
