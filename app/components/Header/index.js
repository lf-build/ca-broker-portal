/**
*
* Header
*
*/

import React from 'react';
import logo from 'assets/images/logo.png';
import { Link } from 'react-router';
import { userSignedOut } from 'sagas/auth/actions';
import { connect } from 'react-redux';
// import styled from 'styled-components';


function Header(props) {
  const { emailMode, dispatch } = props;
  return (
    <header>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-3 col-xs-8">{emailMode ? <img alt="logo" src={logo} /> : <Link to={'/dashboard/active'} className="logo"><img alt="logo" src={logo} /></Link>}</div>
          <div className="col-md-8 col-sm-2 "></div>
          {emailMode ? <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 pull-right"><div className="question_call"> <i className="fa fa-phone" aria-hidden="true"></i> Questions? Call us (888) 381-0532</div></div> : <div className="col-md-1 col-sm-4"><Link onClick={() => dispatch(userSignedOut())} className="logout_btn">Logout</Link></div>}
        </div>
      </div>
    </header>
  );
}

Header.defaultProps = {
  emailMode: false,
};
Header.propTypes = {
  emailMode: React.PropTypes.bool,
  dispatch: React.PropTypes.func,
};
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(Header);
