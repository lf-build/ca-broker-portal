/**
*
* PrivacyPolicy
*
*/

import React from 'react';
import ssl from '../../assets/images/ssl.png';
import soc from '../../assets/images/soc.jpg';
// import styled from 'styled-components';


class PrivacyPolicy extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className="col-md-12">
        <div className="privacy-wrap">
          <div className="privacy-imgs col-sm-3">
            <div className="pri-logo">
              <img src={soc} alt="" />
            </div>
            <div className="ssl-logo">
              <img src={ssl} alt="SSL" />
            </div>
          </div>
          <div className="privacy-text col-sm-9">
            <h4>Privacy notice</h4>
            <p>Your information is safe , secure, and private. Divergent capital does not view or store your login information. if you’d like to learn more about this process, please check our <a> Account linking FAQ</a></p>
          </div>
        </div>
      </div>
    );
  }
}

PrivacyPolicy.propTypes = {

};

export default PrivacyPolicy;
