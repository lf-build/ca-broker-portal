/**
*
* AccountLinkingDone
*
*/

import React from 'react';
import allDone from '../../assets/images/all_done.png';
// import styled from 'styled-components';


class AccountLinkingDone extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <div className="que-heading">
          <h2>Business Verification</h2>
        </div>
        <div className="col-md-12">
          <div className="all-done">
            <img src={allDone} alt="" />
            <h3 className="done-heading">All Done</h3>
            <p className="done-text">Thank you for submitting your information. Your application is being reviewed by an underwriter - a process which usually takes up to 24 hours You may reach out to your broker for more information. </p>
          </div>
        </div></div>
    );
  }
}

AccountLinkingDone.propTypes = {

};

export default AccountLinkingDone;
