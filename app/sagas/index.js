// import AppSaga from '../containers/App/sagas';
import sagas from '@sigma-infosolutions/uplink/sagas/uplink';
import authSaga from './auth';
export default [
  ...sagas,
  ...authSaga,
];
