import isEmpty from './isEmpty';

export default (a, start, end, excluding) => {
  if (isEmpty(a)) {
    return true;
  }
  let value = a;
  if (typeof a === 'string') {
    value = a.toString().length;
  }
  if (excluding) {
    return start < value && value < end;
  }
  return start <= value && value <= end;
};
