import isInRange from './isInRange';
export const isInLength = (min, max, exclude) => (value) => isInRange(value, min, max, exclude) ? undefined : `Filed length must be between ${min} and ${max}`;
