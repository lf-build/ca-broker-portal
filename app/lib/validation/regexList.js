export const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
export const postalCodeRegex = /[0-9]{5}$/i;
export const mobileRegex = /^[+]?[(]?[0-9]{3}[)]?[-s.]?[0-9]{3}[-s.]?[0-9]{4,6}$/i;
export const ssnRegex = /^[0-9]{3}-?[0-9]{2}-?[0-9]{4}$/i;
