import _isEqual from './isEqual';
import _isInRange from './isInRange';
import _isEmpty from './isEmpty';
import _isRegexMatch from './isRegexMatch';

export const isEqual = _isEqual;
export const isInRange = _isInRange;
export const isEmpty = _isEmpty;
export const isRegexMatch = _isRegexMatch;
