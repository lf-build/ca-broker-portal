import isRegexMatch from './isRegexMatch';
import { emailRegex } from './regexList';

export const isEmail = (name) => (value) => isRegexMatch(value, emailRegex) ? undefined : `Invalid ${name}`;
