/*
 * ResetAccount Messages
 *
 * This contains all the text for the ResetAccount component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ResetAccount.header',
    defaultMessage: 'This is ResetAccount container !',
  },
  passwordPlaceholder: {
    id: 'app.components.ResetAccount.passwordPlaceholder',
    defaultMessage: 'New Password',
  },
  confirmPasswordPlaceholder: {
    id: 'app.components.ResetAccount.confirmPasswordPlaceholder',
    defaultMessage: 'Confirm Password',
  },
});
