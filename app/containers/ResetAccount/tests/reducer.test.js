
import { fromJS } from 'immutable';
import resetAccountReducer from '../reducer';

describe('resetAccountReducer', () => {
  it('returns the initial state', () => {
    expect(resetAccountReducer(undefined, {})).toEqual(fromJS({}));
  });
});
