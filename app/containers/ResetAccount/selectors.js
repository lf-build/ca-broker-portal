import { createSelector } from 'reselect';

/**
 * Direct selector to the resetAccount state domain
 */
const selectResetAccountDomain = () => (state) => state.get('resetAccount');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ResetAccount
 */

const makeSelectResetAccount = () => createSelector(
  selectResetAccountDomain(),
  (substate) => substate.toJS()
);

export default makeSelectResetAccount;
export {
  selectResetAccountDomain,
};
