
import { fromJS } from 'immutable';
import accountLinkingReducer from '../reducer';

describe('accountLinkingReducer', () => {
  it('returns the initial state', () => {
    expect(accountLinkingReducer(undefined, {})).toEqual(fromJS({}));
  });
});
