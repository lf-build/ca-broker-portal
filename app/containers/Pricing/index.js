/*
 *
 * Pricing
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory, withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import Layout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import { authCheck } from 'components/Helper/authCheck';
import Breadcrumb from 'components/Breadcrumb';
import ApplicantDetail from 'components/ApplicantDetail';
import PricingBox from 'components/PricingBox';
import AlertMessage from 'components/AlertMessage';
import { createStructuredSelector } from 'reselect';
import makeSelectPricing from './selectors';

// Calculate max daily payment based on data received on page load
const calculateDailyRepayment = (monthlyRevenue, avgDailyBalance,
    maxDailyPayment, minDailyPayment) => {
  let dailyRepayment = 0.0;
  const dailyRevanue = parseFloat(monthlyRevenue) / 20;
  if (dailyRevanue < parseFloat(avgDailyBalance)) {
    dailyRepayment = dailyRevanue * maxDailyPayment;
  } else {
    dailyRepayment = avgDailyBalance * minDailyPayment;
  }
  return Math.round(dailyRepayment);
};

export class Pricing extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isError: false,
      message: '',
      isVisible: false,
    };
    this.debouncedDailyPaymentHandler = this.debounce('dp', 500, this.changeDailyPayment);
    this.debouncedSellRateHandler = this.debounce('sr', 500, this.changeSellRate);
    this.debouncedTermHandler = this.debounce('t', 500, this.changeTerm);
  }

  componentWillMount() {
    execute(undefined, undefined, ...'on-boarding-broker/pricing/get-offers'.split('/'), {
      applicationNumber: `${this.props.location.pathname.split('/')[2]}`,
    })
    .then(({ body }) => {
      this.setState({ pricing: body });
      // Following statements set initial daily payment, sellRate and term values
      this.state.pricing.dailyPayment = 0;
      this.state.pricing.sellRate = this.state.pricing.range.sellRate.min;
      this.state.pricing.term = this.state.pricing.range.term.min;
      this.setState(this.state);
    }).catch((e) => {
      authCheck(e);
      this.state.isError = true;
      this.state.message = 'Something went wrong!';
      this.state.isVisible = true;
      this.setState(this.state);
    });
  }
  calculateIntrimOffer=() => {
    const payload = { ...this.state.pricing };
    execute(undefined, undefined, ...'on-boarding-broker/pricing/get-intrim-offers'.split('/'), {
      ...payload,
    })
    .then(({ body }) => {
      this.updateState(body);
    }).catch((e) => {
      authCheck(e);
      this.state.isError = true;
      this.state.message = 'Something went wrong!';
      this.state.isVisible = true;
      this.setState(this.state);
    }); // eslint-disable-line
  }

  updateState =(body) => {
    const { amountFunded,
                 amountPayback,
                 commission,
                 commissionPercentage,
                 divergentReturn,
                 divergentFR,
                 afterDefaultAssumption,
                 averageLife,
                 iir,
                 durationIncremental,
                 sellIncremental,
                 monthlyRevenue,
                 averageDailyBalance,
                 durationType } = body;
    this.setState({
      pricing: {
        ...this.state.pricing,
        amountFunded: Number.isNaN(amountFunded) ? 0 : amountFunded,
        amountPayback: Number.isNaN(amountPayback) ? 0 : amountPayback,
        commission: Number.isNaN(commission) ? 0 : commission,
        commissionPercentage: Number.isNaN(commissionPercentage) ? 0 : commissionPercentage,
        divergentReturn: Number.isNaN(divergentReturn) ? 0 : divergentReturn,
        divergentFR: Number.isNaN(divergentFR) ? 0 : divergentFR,
        afterDefaultAssumption: Number.isNaN(afterDefaultAssumption) ? 0 : afterDefaultAssumption,
        averageLife: Number.isNaN(averageLife) ? 0 : averageLife,
        iir: Number.isNaN(iir) ? 0 : iir,
        durationIncremental: Number.isNaN(durationIncremental) ? 0 : durationIncremental,
        sellIncremental: Number.isNaN(sellIncremental) ? 0 : sellIncremental,
        monthlyRevenue: Number.isNaN(monthlyRevenue) ? 0 : monthlyRevenue,
        averageDailyBalance: Number.isNaN(averageDailyBalance) ? 0 : averageDailyBalance,
        durationType,
      },
    });
  };

  // Add final deal
  sendContract=() => {
    const payload = { ...this.state.pricing };
    this.state.loading = true;
    this.setState(this.state);
    delete payload.range;
    execute(undefined, undefined, ...'on-boarding-broker/pricing/set-deal'.split('/'), {
      ...payload,
    })
    .then(() => {
      browserHistory.replace(`/agreement/${this.props.location.pathname.split('/')[2]}`);
    }).catch((e) => {
      authCheck(e);
      this.state.loading = false;
      this.state.isError = true;
      this.state.message = 'Something went wrong!';
      this.state.isVisible = true;
      this.setState(this.state);
    }); // eslint-disable-line
  }

  maximizeCommision=() => {
    this.state.pricing.sellRate = this.state.pricing.range.sellRate.max; // Pick maximum sell rate for maximizing commision
    this.sellRateInput.value = this.state.pricing.range.sellRate.max;
    this.calculateIntrimOffer();
  }

  maximizeAdvance=() => {
    this.state.pricing.sellRate = this.state.pricing.range.sellRate.min; // Pick minimum sell rate for maximizining advance
    this.sellRateInput.value = this.state.pricing.range.sellRate.min;
    this.calculateIntrimOffer();
  }

  // Daily payment slider change event
  changeDailyPayment=(dailyPayment) => {
    this.state.pricing.dailyPayment = dailyPayment;
    this.calculateIntrimOffer();
  }

  // Sellrate slider change event
  changeSellRate=(sellRate) => {
    this.state.pricing.sellRate = sellRate;
    this.calculateIntrimOffer();
  }

  // Term slider change event
  changeTerm=(term) => {
    this.state.pricing.term = term;
    this.calculateIntrimOffer();
  }

  // Debounce logic
  debounce(key, timeout, cb) {
    return (value) => {
      if (this[key]) {
        clearTimeout(this[key]);
      }
      this[key] = setTimeout(() => {
        this[key] = undefined;
        cb(value);
      }, timeout);
    };
  }

  render() {
    if (!this.state || !this.state.pricing) {
      return <Spinner />;
    }
    const { monthlyRevenue, averageDailyBalance, range } = this.state.pricing;
    const maxDailyPayment = calculateDailyRepayment(monthlyRevenue, averageDailyBalance, range.dailyPayment.max, range.dailyPayment.min);
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <Breadcrumb applicationStatus="2" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Client Verified</b>
                <p>
                  {'It\'s time to negotiate a deal.Use the sliders below to adjust to your client\'s needs.'}
                </p>
              </div>
            </div>
            <div>
              <div className="col-sm-12">
                <div className="divider"></div>
              </div>
              <div className="col-md-10 col-sm-12">
                <div className="pricing_stage_wraper">
                  <div className="col-md-6 col-sm-12 no-pad">
                    <PricingBox {...this.state.pricing} />
                  </div>
                  <div className="col-md-6 col-sm-12 no-pad">
                    <div className="dailypemant_box">
                      <div className="span6">
                        <div className="dailypemant">
                          <strong>Daily Payment</strong>
                          <div className="prog-wrap">
                            <input disabled={this.state.loading} defaultValue={0} onChange={(e) => this.debouncedDailyPaymentHandler(e.target.value)} name="s_dailyPayment" type="range" min="0" max={maxDailyPayment} step="1" />
                            <span className="pull-right range-right">{`$${calculateDailyRepayment(monthlyRevenue, averageDailyBalance, range.dailyPayment.max, range.dailyPayment.min)}`}</span>
                            <span className="pull-left range-left">$0 </span>
                            <p>{this.state.pricing.dailyPayment}</p>
                          </div>
                          <div className="sell_reat">
                            <strong> Sell Rate</strong>
                            <input disabled={this.state.loading} ref={(input) => { this.sellRateInput = input; }} defaultValue={range.sellRate.min} onChange={(e) => this.debouncedSellRateHandler(e.target.value)} name="s_sellRate" type="range" min={range.sellRate.min} max={range.sellRate.max} step="0.01" />
                            <span className="pull-right range-right">{range.sellRate.max}</span>
                            <span className="pull-left range-left">{range.sellRate.min}</span>
                            <p>{this.state.pricing.sellRate}</p>
                          </div>
                          <div className="term">
                            <strong> Term</strong>
                            <input disabled={this.state.loading} ref={(input) => { this.termInput = input; }} defaultValue={range.term.min} onChange={(e) => this.debouncedTermHandler(e.target.value)} name="s_term" type="range" min={range.term.min} max={range.term.max} step="1" />
                            <span className="pull-right range-right">{range.term.max}</span>
                            <span className="pull-left range-left">{range.term.min}</span>
                            <p>{this.state.pricing.term}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-2 col-sm-4 no-pad">

                </div>
              </div>
              <div className="col-sm-12">
                <div className="divider"></div>
              </div>
              <div className="col-sm-12">
              </div>
              <div className="col-sm-12">
                <div className="col-md-8">
                  <div className="row">
                    {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
                  </div> </div></div>
              <div className="maxmixe_wraper">
                <div className="row">
                  <div className="col-lg-10 col-md-10 col-sm-12 col-xs-12 no-pad">
                    <div className=" col-lg-2 col-md-2 col-sm-4 col-xs-6">
                      <p className="maximize_text">Maximize :</p>
                    </div>
                    <div className=" col-lg-2 col-md-2 col-sm-4 col-xs-12">
                      <button disabled={this.state.loading} type="button" className="maxmixe_btn" onClick={() => this.maximizeCommision()}> Commission</button>
                    </div>
                    <div className=" col-lg-2 col-md-2 col-sm-4 col-xs-12">
                      <button disabled={this.state.loading} type="button" className="maxmixe_btn advance_btn" onClick={() => this.maximizeAdvance()}> Advance</button>
                    </div>
                    <div className=" col-lg-3 col-md-2 col-sm-4 col-xs-12">
                    </div>
                    <div className=" col-lg-3 col-md-4 col-sm-4 col-xs-12">
                      <button disabled={this.state.loading} type="button" className="generatr_deal" onClick={() => this.sendContract()}> {this.state.loading ? 'SENDING ...' : 'SEND CONTRACT'} </button>
                    </div>
                  </div>
                  <div className="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Pricing.propTypes = {
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  pricing: makeSelectPricing(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Pricing));
