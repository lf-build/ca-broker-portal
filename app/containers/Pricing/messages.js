/*
 * Pricing Messages
 *
 * This contains all the text for the Pricing component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Pricing.header',
    defaultMessage: 'This is Pricing container !',
  },
});
