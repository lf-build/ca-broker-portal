/*
 *
 * Declined
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import Layout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import Breadcrumb from 'components/Breadcrumb';
import FinalApplicationDetail from 'components/FinalApplicationDetail';
import ApplicantDetail from 'components/ApplicantDetail';
import { authCheck } from 'components/Helper/authCheck';
import img from 'assets/images/error-icon.png';
import { createStructuredSelector } from 'reselect';
import makeSelectDeclined from './selectors';

export class Declined extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.location.pathname.split('/')[2] };
  }
  componentDidMount() {
    execute(undefined, undefined, ...'on-boarding-broker/declined/get-declined'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
    .then(({ body }) => {
      this.setState({ declineReasons: body.declineReasons, applicationDetails: body.applicationDetails });
      return body;
    }).catch((e) => {
      authCheck(e);
    });
  }
  render() {
    if (!this.state || !this.state.declineReasons) {
      return <Spinner />;
    }
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="4" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Application disqualified</b>
                <p>Unfortunately, your application was not approved for funding at this time. <br />You may resubmit the application should there be a positive change in status for the client.</p>
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="finalization-wrap">
              <div className="col-lg-10 no-pad">
                <div className="final-head">
                  <h4>{'Here\'s your deal summary:'}</h4>
                </div>
                <div className="col-lg-5 col-sm-6 ">
                  <FinalApplicationDetail applicationDetails={this.state.applicationDetails} />
                </div>
                <div className="col-lg-5 col-sm-6">
                  <div className="dis-agreement">
                    <div className="icon-err">
                      <img src={img} alt="" />
                    </div>
                    <div className="disq-text">
                      <h3>Application disqualified for the following reasons:</h3>
                      <ul className="error-msgs">
                        {this.state.declineReasons.map((declineReason, index) => (
                          <li key={index}><h5>{declineReason.reason}</h5>
                            <p>{declineReason.subReason}</p>
                          </li>
                      ))}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Declined.propTypes = {
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Declined: makeSelectDeclined(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Declined));
