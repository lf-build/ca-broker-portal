/*
 *
 * Finalization
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import Layout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import Breadcrumb from 'components/Breadcrumb';
import FinalApplicationDetail from 'components/FinalApplicationDetail';
import ApplicantDetail from 'components/ApplicantDetail';
import { authCheck } from 'components/Helper/authCheck';
import img from 'assets/images/agreement-icon.png';
import { createStructuredSelector } from 'reselect';
import makeSelectFinalization from './selectors';

export class Finalization extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.location.pathname.split('/')[2] };
  }
  componentDidMount() {
    execute(undefined, undefined, ...'on-boarding-broker/finalization/get-finalization'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
    .then(({ body }) => {
      this.setState({ Finalization: body });
      return body;
    }).catch((e) => {
      authCheck(e);
    });
  }
  render() {
    if (!this.state || !this.state.Finalization) {
      return <Spinner />;
    }
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="4" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Thanks for submitting the agreement!</b>
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="finalization-wrap">
              <div className="col-lg-10 no-pad">
                <div className="final-head">
                  <h4>{'Here\'s your deal summary:'}</h4>
                </div>
                <div className="col-lg-5 col-sm-6 ">
                  <FinalApplicationDetail applicationDetails={this.state.Finalization} />
                </div>
                <div className="col-lg-5 col-sm-6">
                  <div className="submit-agreement">
                    <div className="icon-bx">
                      <img src={img} alt="" />
                    </div>
                    <div className="agree-text">
                      <h3>What happens now?</h3>
                      <p>Your application is currently being processed for funding. This process usually takes up to 24 hours. Once it is done, the funds will be disbursed to your client’s account.</p>
                      <a href="/dashboard">Back to Dashboard</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Finalization.propTypes = {
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Finalization: makeSelectFinalization(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Finalization));
