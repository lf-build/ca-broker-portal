
import { fromJS } from 'immutable';
import finalizationReducer from '../reducer';

describe('finalizationReducer', () => {
  it('returns the initial state', () => {
    expect(finalizationReducer(undefined, {})).toEqual(fromJS({}));
  });
});
