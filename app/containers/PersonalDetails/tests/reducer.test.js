
import { fromJS } from 'immutable';
import personalDetailsReducer from '../reducer';

describe('personalDetailsReducer', () => {
  it('returns the initial state', () => {
    expect(personalDetailsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
