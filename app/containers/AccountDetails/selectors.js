import { createSelector } from 'reselect';

/**
 * Direct selector to the accountDetails state domain
 */
const selectAccountDetailsDomain = () => (state) => state.get('accountDetails');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AccountDetails
 */

const makeSelectAccountDetails = () => createSelector(
  selectAccountDetailsDomain(),
  (substate) => substate.toJS()
);

export default makeSelectAccountDetails;
export {
  selectAccountDetailsDomain,
};
