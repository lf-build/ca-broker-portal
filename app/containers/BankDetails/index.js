/*
 *
 * BankDetails
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Layout from 'containers/AppLayout';
import makeSelectBankDetails from './selectors';
import BankDetailsSetting from '../../components/BankDetailsSetting';

export class BankDetails extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Layout>
        <div className="application_wraper">
          <BankDetailsSetting />
        </div>
      </Layout>
    );
  }
}

BankDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  BankDetails: makeSelectBankDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BankDetails);
