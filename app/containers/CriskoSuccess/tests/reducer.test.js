
import { fromJS } from 'immutable';
import criskoSuccessReducer from '../reducer';

describe('criskoSuccessReducer', () => {
  it('returns the initial state', () => {
    expect(criskoSuccessReducer(undefined, {})).toEqual(fromJS({}));
  });
});
