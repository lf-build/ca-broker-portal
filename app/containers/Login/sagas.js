import { takeEvery, cancel, take } from 'redux-saga/effects';
import { browserHistory } from 'react-router';

function* redirectToDashbBoard() {
  browserHistory.replace('/dashboard');
}

// Individual exports for testing
export function* defaultSaga() {
  const watcher = yield takeEvery('app/uplink/EXECUTE_UPLINK_REQUEST_ENDED_who-am-i', redirectToDashbBoard);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
