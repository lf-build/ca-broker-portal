
import { fromJS } from 'immutable';
import fundedReducer from '../reducer';

describe('fundedReducer', () => {
  it('returns the initial state', () => {
    expect(fundedReducer(undefined, {})).toEqual(fromJS({}));
  });
});
