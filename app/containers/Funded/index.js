/*
 *
 * Funded
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import Layout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import Breadcrumb from 'components/Breadcrumb';
import FinalApplicationDetail from 'components/FinalApplicationDetail';
import ApplicantDetail from 'components/ApplicantDetail';
import { authCheck } from 'components/Helper/authCheck';
import img from 'assets/images/success-icon.png';
import { createStructuredSelector } from 'reselect';
import makeSelectFunded from './selectors';

export class Funded extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.location.pathname.split('/')[2] };
  }
  componentDidMount() {
    execute(undefined, undefined, ...'on-boarding-broker/finalization/get-finalization'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
    .then(({ body }) => {
      this.setState({ Funded: body });
      return body;
    }).catch((e) => {
      authCheck(e);
    });
  }
  render() {
    if (!this.state || !this.state.Funded) {
      return <Spinner />;
    }
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="4" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Deal funded successfully</b>
                <p>your client, {this.state.Funded.businessName}, was funded successfully. See below for deal summary.</p>
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="finalization-wrap">
              <div className="col-lg-10 no-pad">
                <div className="final-head">
                  <h4>{'Here\'s your deal summary:'}</h4>
                </div>
                <div className="col-lg-5 col-sm-6 ">
                  <FinalApplicationDetail applicationDetails={this.state.Funded} />
                </div>
                <div className="col-lg-5 col-sm-6">
                  <div className="success-agreement">
                    <div className="icon-bx">
                      <img src={img} alt="" />
                    </div>
                    <div className="agree-text">
                      <h3>Deal funded successfully</h3>
                      <a href="/dashboard">Back to Dashboard</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Funded.propTypes = {
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Funded: makeSelectFunded(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Funded));
