/*
 *
 * Verification
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import Layout from 'containers/AppLayout';
import { createStructuredSelector } from 'reselect';
import Spinner from 'components/Spinner';
import Breadcrumb from 'components/Breadcrumb';
import ApplicantDetail from 'components/ApplicantDetail';
import ClientEmailVerification from 'components/ClientEmailVerification';
import ClientAccountLinking from 'components/ClientAccountLinking';
import UnderWritingStatus from 'components/UnderWritingStatus';
import { simplePhoneMasking } from 'components/Helper/formatting';
import { authCheck } from 'components/Helper/authCheck';
import makeSelectVerification from './selectors';

export class Verification extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      applicationNumber: this.props.location.pathname.split('/')[2],
    };
  }
  componentDidMount() {
    execute(undefined, undefined, ...'on-boarding-broker/verification/get-verification'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
    .then(({ body }) => {
      this.setState({ Verification: body, applicationNumber: this.props.location.pathname.split('/')[2] });
    }).catch((e) => {
      authCheck(e);
    });
  }
  render() {
    if (!this.state || !this.state.Verification) {
      return <Spinner />;
    }
    const { emailStatus, phoneNumber, bankLinkingStatus, underWritingStatus } = this.state.Verification;
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="1" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Linking your client’s accounts</b>
                <p>Help your client get approved by having them connect their Accounting System &amp; Bank Account.</p>
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="verification-wrap">
              <div className="col-lg-10 no-pad">
                <div className="col-sm-4">
                  <ClientEmailVerification phoneNumber={simplePhoneMasking(phoneNumber)} emailStatus={emailStatus} />
                </div>
                <div className="col-sm-4">
                  <ClientAccountLinking applicationNumber={this.state.applicationNumber} bankLinkingStatus={bankLinkingStatus} />
                </div>
                <div className="col-sm-4">
                  <UnderWritingStatus underWritingStatus={underWritingStatus} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Verification.propTypes = {
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Verification: makeSelectVerification(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Verification));
