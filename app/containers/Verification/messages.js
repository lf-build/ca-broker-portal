/*
 * Verification Messages
 *
 * This contains all the text for the Verification component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Verification.header',
    defaultMessage: 'This is Verification container !',
  },
});
