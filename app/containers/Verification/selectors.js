import { createSelector } from 'reselect';

/**
 * Direct selector to the verification state domain
 */
const selectVerificationDomain = () => (state) => state.get('verification');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Verification
 */

const makeSelectVerification = () => createSelector(
  selectVerificationDomain(),
  (substate) => substate.toJS()
);

export default makeSelectVerification;
export {
  selectVerificationDomain,
};
